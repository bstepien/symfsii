<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SearchPhrase;
use AppBundle\Form\Search;
use AppBundle\ResponseTransformer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WikiController extends Controller
{

    /**
     * Show the form
     *
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function searchAction(Request $request)
    {

        $form = $this->createForm(Search::class, new SearchPhrase());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $url = $form->get('article_url')->getData();

            return $this->redirect($url);
        }

        return $this->render('wiki/search.html.twig',
            [
                'form' => $form->createView(),
            ]);
    }


    /**
     * @Route("/find/phrase/{phrase}", name="find_autocomplete")
     * @Method("GET")
     * @param string $phrase
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function findAction($phrase, Request $request)
    {

        if (! $request->isXmlHttpRequest()) {
            return new JsonResponse(['message' => 'Ajax only!'], Response::HTTP_BAD_REQUEST);
        }

        $data_content = $this->get('wiki.search')->search($phrase);

        $transformer = new ResponseTransformer(json_decode($data_content, true));
        $articles    = $transformer->transformData();

        return $this->json($articles);

    }
}
