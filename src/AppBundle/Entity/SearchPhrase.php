<?php


namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class SearchPhrase
{

    /**
     *
     * @Assert\NotBlank(
     *    message = "There is no results for searched phrase",
     * )
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     *
     * @var string
     */
    public $article_url;

    /**
     * @Assert\NotBlank(
     *     message = "The title is empty",
     * )
     *
     * @var string
     */
    public $article_title;

    /**
     * @return string
     */
    public function getArticleTitle()
    {
        return $this->article_title;
    }

    /**
     * @param $article_title
     * @return $this
     */
    public function setArticleTitle($article_title)
    {
        $this->article_title = $article_title;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticleUrl()
    {
        return $this->article_url;
    }

    /**
     * @param $article_url
     * @return $this
     */
    public function setArticleUrl($article_url)
    {
        $this->article_url = $article_url;

        return $this;
    }
}
