$(function () {

    $("#search_article_title").autocomplete({
        minLength: 3,
        search: function(event, ui) {
            $("#loading").show();
            $("#search_article_url").val(null);
        },
        response: function(event, ui) {
            $("#loading").hide();
        },
        source: function (request, response) {

            $("#loading").show();

            $.ajax({
                type: 'GET',
                url: find_url.replace("phrase_to_replace", request.term),
                dataType: 'json',
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            title: item.title,
                            url: item.url,
                            content: item.content,
                        };
                    }));
                },
                error: function () {
                    $("#loading").hide();
                }
            })
        },
        focus: function (event, ui) {
            $("#search_article_title").val(ui.item.title);
            $("#search_article_url").val(ui.item.url);
            return false;
        },
        select: function (event, ui) {
            $("#search_article_title").val(ui.item.title);
            $("#search_article_url").val(ui.item.url);

            return false;
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
            .append('<div class="drop-down-list"><strong>' + item.title + '</strong><br>' + item.content + '</div>')
            .appendTo(ul);
    };
    ;
});
