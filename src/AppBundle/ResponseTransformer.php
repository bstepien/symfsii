<?php

namespace AppBundle;


class ResponseTransformer
{

    const NAME_ID = 1;
    const DESC_ID = 2;
    const URL_ID  = 3;

    /**
     * @param array $content_data
     */
    public function __construct(array $content_data)
    {
        $this->content_data = $content_data;
    }

    /**
     * @return array
     */
    public function transformData()
    {
        if (! $this->hasResults()) {
            return null;
        }

        foreach ($this->content_data[self::NAME_ID] as $key => $data) {

            $title   = $this->content_data[self::NAME_ID][$key];
            $content = $this->content_data[self::DESC_ID][$key];
            $url     = $this->content_data[self::URL_ID][$key];

            $results[] = new Article($title, $content, $url);
        }

        return $results;
    }

    /**
     * @return bool
     */
    private function hasResults()
    {
        return ! empty($this->content_data[self::NAME_ID]);
    }
}
