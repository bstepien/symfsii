<?php

namespace AppBundle;

use GuzzleHttp\Client;

class Search
{

    /**
     * @var string
     */
    public $endpoint = 'en.wikipedia.org/w/api.php';

    /**
     * @var string
     */
    public $format = 'json';

    /**
     * @var string
     */
    public $action = 'opensearch';

    /**
     * @var integer
     */
    public $limit = 15;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->url = 'http://' . $this->endpoint
            . '?format=' . $this->format
            . '&action=' . $this->action
            . '&limit=' . $this->limit
            . '&namespace=0';
    }

    /**
     * Retrieves articles by phrase
     *
     * @param string $phrase
     * @return string
     */
    public function search($phrase)
    {
        $query = $this->url . '&search=' . $phrase;

        return $this->send($query);
    }

    /**
     * @param string $query
     * @return string
     */
    protected function send($query)
    {
        $response = $this->client->get($query);

        return $response->getBody()->getContents();
    }
}
